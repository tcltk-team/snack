Source: snack
Section: sound
Priority: optional
Maintainer: Sergei Golovan <sgolovan@debian.org>
Build-Depends: debhelper-compat (= 13), tk-dev (>= 8.5), libmpg123-dev, libogg-dev, libvorbis-dev,
 libasound2-dev [linux-any]
Build-Depends-Indep: python3-all-dev, python3-setuptools, dh-python
Standards-Version: 4.7.0
Homepage: http://pdqi.com/w/pw/pdqi/Wize/Snack
Vcs-Browser: https://salsa.debian.org/tcltk-team/snack
Vcs-Git: https://salsa.debian.org/tcltk-team/snack.git

Package: tcl-snack
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${tclsh:Depends},
 libsnack-oss (= ${binary:Version}) | libsnack-alsa (= ${binary:Version})
Suggests: ${wish:Depends}, tcl-snack-doc
Conflicts: libsnack2, libsnack2-alsa
Provides: libsnack2, libsnack2-alsa
Replaces: libsnack2, libsnack2-alsa
Description: Sound extension to Tcl/Tk and Python/Tkinter - Tcl/Tk library
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.

Package: libsnack-alsa
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libsnack-oss
Replaces: libsnack2, libsnack2-alsa, libsnack-oss
Description: Sound extension to Tcl/Tk and Python/Tkinter - ALSA files
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.
 .
 This package includes libraries which use ALSA as a sound engine.

Package: libsnack-oss
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, osspd | oss-compat
Conflicts: libsnack-alsa
Replaces: libsnack2, libsnack2-alsa, libsnack-alsa
Description: Sound extension to Tcl/Tk and Python/Tkinter - OSS files
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.
 .
 This package includes libraries which use OSS as a sound engine.

Package: python3-tksnack
Section: python
Architecture: all
Depends: tcl-snack (>= ${source:Version}), ${python3:Depends}, ${misc:Depends}, python3-tk
Suggests: tcl-snack-doc
Provides: ${python3:Provides}
Description: Sound extension to Tcl/Tk and Python/Tkinter - Python 3.x library
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.
 .
 This package includes Python/Tkinter tkSnack package for Python 3.x.

Package: tcl-snack-dev
Section: libdevel
Architecture: any
Depends: tcl-snack (= ${binary:Version}), ${misc:Depends}
Replaces: snack-dev, libsnack2-dev
Conflicts: libsnack2-dev
Provides: libsnack2-dev
Description: Sound extension to Tcl/Tk and Python/Tkinter - development files
 This package is needed for building transcriber, and contains
 snackConfig.sh, snack.h.
 .
 Snack provides a sound functionality extension to the Tcl/Tk language.
 .
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.
 .
 This package contains files to be used for development of C-based
 extensions which use the Snack library.

Package: tcl-snack-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: tcl-snack, python3-tksnack
Replaces: libsnack2-doc
Conflicts: libsnack2-doc
Provides: libsnack2-doc
Description: Sound extension to Tcl/Tk and Python/Tkinter - documentation
 Snack provides a sound functionality extension to the Tcl/Tk language.
 .
 Snack is an extension to the Tcl/Tk scripting language
 that adds sound functionality. There are commands to play, record,
 edit, and even visualize sound. Snack supports in-memory sound
 objects, file based audio, and streaming audio. It handles
 file formats such as WAV, AU, AIFF, MP3, and OGG Vorbis.
 .
 This package contains the HTML documentation for Snack.
